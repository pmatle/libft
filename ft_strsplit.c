/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/03 15:19:37 by pmatle            #+#    #+#             */
/*   Updated: 2017/08/24 11:47:47 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		count_words(char const *str, char c)
{
	int		x;
	int		y;
	int		word;

	x = 0;
	y = 0;
	word = 0;
	if (ft_strchr(str, c) == NULL)
		return (1);
	while (str[x])
	{
		while (str[x] != c && str[x])
		{
			y++;
			x++;
		}
		if (y != 0 && ((str[x] == c) || str[x] == '\0'))
		{
			y = 0;
			word++;
		}
		if (str[x] != '\0')
			x++;
	}
	return (word);
}

static int		word_len(char const *str, char c)
{
	int		x;
	int		y;

	x = 0;
	y = 0;
	while (str[x] != c && str[x] != '\0')
	{
		x++;
		y++;
	}
	return (y);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**str;
	int		words;
	int		x;

	x = 0;
	if (s == NULL)
		return (NULL);
	words = count_words(s, c);
	if (!(str = (char **)malloc(sizeof(*str) * (words + 1))))
		return (NULL);
	if (s[0] == '\0')
		str[x] = ft_strsub(s, 0, word_len(s, c));
	while (words && s[0] != '\0')
	{
		while (*s == c && *s)
			s++;
		str[x] = ft_strsub(s, 0, word_len(s, c));
		s = s + word_len(s, c);
		x++;
		words--;
	}
	str[x] = NULL;
	return (str);
}
