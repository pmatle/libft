/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/17 12:06:14 by pmatle            #+#    #+#             */
/*   Updated: 2017/08/18 12:01:04 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi_base(const char *str, int str_base)
{
	int		len;
	int		ans;
	int		x;
	int		val;

	x = 0;
	len = ft_strlen(str) - 1;
	ans = 0;
	if (str_base >= 2 && str_base <= 16)
	{
		while (str[x] != '\0')
		{
			if (str[x] >= 48 && str[x] <= 57)
				val = str[x] - 48;
			else if (str[x] >= 97 && str[x] <= 102)
				val = str[x] - 87;
			ans = ans + (val * ft_power(str_base, len));
			len--;
			x++;
		}
		return (ans);
	}
	return (0);
}
