/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_power.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/17 12:16:37 by pmatle            #+#    #+#             */
/*   Updated: 2017/08/17 12:34:19 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_power(int value, int power)
{
	if (power == 0)
		return (1);
	while (power != 1)
	{
		value = value * value;
		power--;
	}
	return (value);
}
