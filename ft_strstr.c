/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 19:49:48 by pmatle            #+#    #+#             */
/*   Updated: 2017/07/26 14:12:57 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	int		x;
	int		y;

	x = 0;
	if (ft_strlen(little) == 0)
		return ((char*)big);
	while (big[x] != '\0')
	{
		y = 0;
		while (big[x + y] == little[y])
		{
			y++;
			if (little[y] == '\0')
				return ((char*)(big + x));
		}
		x++;
	}
	return (NULL);
}
