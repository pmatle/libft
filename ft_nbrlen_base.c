/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nbrlen_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/17 09:10:57 by pmatle            #+#    #+#             */
/*   Updated: 2017/08/17 09:14:50 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_nbrlen_base(long long value, int base)
{
	int		x;

	x = 0;
	if (value < 0 && base == 10)
		x++;
	if (value < 0)
		value = value * -1;
	while (value)
	{
		value = value / base;
		x++;
	}
	return (x);
}
