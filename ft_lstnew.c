/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/24 15:56:35 by pmatle            #+#    #+#             */
/*   Updated: 2017/08/19 08:18:23 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*link;
	void	*value;

	link = (t_list*)malloc(sizeof(*link));
	value = malloc(content_size);
	if (!link)
		return (NULL);
	if (content)
	{
		link->content = ft_memcpy(value, content, content_size);
		link->content_size = content_size;
	}
	else
	{
		value = NULL;
		link->content = value;
		link->content_size = 0;
	}
	link->next = NULL;
	return (link);
}
