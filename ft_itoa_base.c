/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 08:13:43 by pmatle            #+#    #+#             */
/*   Updated: 2017/08/17 14:26:03 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_itoa_base(long long int value, int base, char c)
{
	int		x;
	char	*str;
	int		answer;

	x = ft_nbrlen_base(value, base);
	if (!(str = ft_strnew(x)))
		return (NULL);
	str[x] = '\0';
	if (value < 0 && base == 10)
		str[0] = '-';
	value = (value < 0) ? value * -1 : value;
	if (value == 0)
		return (ft_strdup("0"));
	while (value)
	{
		answer = value % base;
		if (answer < 10)
			str[x - 1] = answer + 48;
		else
			str[x - 1] = (c == 's') ? (answer + 87) : (answer + 55);
		value = value / base;
		x--;
	}
	return (str);
}
